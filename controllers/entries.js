const mongoose = require('mongoose');
const JWT = require('jsonwebtoken');
const boom = require('boom');
const Entry = require('../models/Entry');
const User = require('../models/User');

exports.getAllUserEntries = async (req, res, err) => {
    const { email } = JWT.decode(req.headers.authorization);
    const entries = await User.findOne({ email: email}).populate('entries').then((model) => {
        return model.entries;   
    });

    return { isSuccess: true, entries };
}

exports.postEntry = async (req, res, err) => {
    const {date, attributes, score } = req.payload;
    const { email } = JWT.decode(req.headers.authorization)

    const entryId = await Entry.create({ date, attributes, score}).then((model) => {
       return model._id 
    });

    await User.update({ email }, {$push: {entries: [entryId]}}).catch((err) => {
        return boom.badRequest(err)
    })

    return { message: 'Entry submitted', entryId}
}

exports.getEntryById = async (req, res, err) => {
    const id = req.params.id
    const entry = await Entry.find({ _id: id }).then((model) => {
        return model;
    }).catch((err) => {
        console.log(err)
    })

    return { entry }
}

exports.updateEntry = async (req, res, err) => {
    const id = req.params.id;
    const {date, attributes, score } = req.payload 

    const updatedEntry = await Entry.update({ _id: id }, {date, attributes, score}).catch((err) => {
        console.log(err)
    });



    return { message: 'Entry has been updated' }
}

exports.deleteEntry = async (req, res, err) => {
    const id = req.params.id;
    const userToken = JWT.decode(req.headers.authorization);
    await Entry.deleteOne({ _id: id}).exec().catch((err) => {
        console.log(err)
    })

    console.log(id)

    const user = await User.findOneAndUpdate({ _id: userToken.id }, { $pull: { entries: id }}).catch((err) => {
        console.log(err)
    })

    await user.entries.remove(id)

    return { message: 'Entry has been successfully deleted', updatedUser: user }
}

