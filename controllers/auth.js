const mongoose = require('mongoose');
const User = require('../models/User');
const Joi = require('joi');
const boom = require('boom');
const JWT = require('jsonwebtoken');
const HapiJWTConfig = require('../config/jsonwebtoken');
const bcrypt = require('bcrypt');

const saltRounds = 10;

exports.authenticate = async function (req, res, err) {
    let isMatch;
    var token;
    let {email, password} = await req.payload;
    console.log(email, password)
    const user = await User.findOne({ email }).then((model) => {
        const {email, admin, _id, password} = model
        return { email, _id, admin, password }  
    }).catch((err) => {
        boom.badRequest(err.message)   
    });

    if(!user) {
        return boom.unauthorized('Invalid Credentials');
    } else {
        isMatch = await bcrypt.compare(password, user.password);
    }

    if(isMatch) {
        return  { token: JWT.sign({ email: user.email, id: user._id }, HapiJWTConfig.key) }
    } else {
        return boom.unauthorized('Invalid Credentials');
    }
}

exports.registerUser = async function (req, res, err) {
    let user;
    const { email, password } = await req.payload;
    const duplicateUser = await User.findOne({ email }).then((model) => {
        return model  
     }).catch((err) => {
         console.log(err)   
     });

     if(duplicateUser) {
        return boom.badRequest("This email already exists. Please try a different email")
     } else {
        user = await User.create({ email, password: await bcrypt.hash(password, saltRounds), admin: false, entries: []}).then((res) => {
            return res
        }).catch((err) => {
            return boom.badRequest(err.message)
        })
     }

    return { token: JWT.sign({ email: user.email, admin: user.admin}, HapiJWTConfig.key), message: 'You have successfully registered!'}
}