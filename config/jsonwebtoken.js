'use strict';

const Users = require('../models/User');
require('dotenv').config()

module.exports = {
    key: process.env.JWT_KEY ,
    validate: async function (decoded, request) { 
        console.log('decoded jwt', decoded)
        const user = await Users.findOne({ email: decoded.email })
        console.log(user)
        if(!user) {
            console.log('jwt invalid')
            return { isValid: false}
        } else {
            return { isValid: true }
        }
    },
    verifyOptions: { algorithms: ['HS256'] }
};