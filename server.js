'use strict';

const Hapi = require('hapi');
const HapiJWTConfig = require('./config/jsonwebtoken')
const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');
const Pack = require('./package');
const mongoose = require('mongoose');
const Path = require('path');
const Joi = require('joi');

const entriesController = require('./controllers/entries');
const authController = require('./controllers/auth');

require('dotenv').config

// Create a server with a host and port
const server= Hapi.server({
    host: '0.0.0.0',
    port: process.env.PORT || 8000,
    routes: {
        cors: {
            origin: ["http://localhost:3000", "https://lasvegas-score.netlify.com"],
            credentials: true
        }
    }
});

mongoose.connect(process.env.DB_CONN)

mongoose.connection.once('open', () => {
    console.log('connected to database')
})


// Start the server
async function start() {
    await server.register([
        Inert,
        Vision,
        {
            plugin: HapiSwagger,
            options: {
                info: {
                    title: 'LASVEGAS API Documentation',
                    version: Pack.version
                }
            }
        },
        {
            plugin: require('hapi-pino'),
            options: {
                prettyPrint: true,
                logEvents: ['response']
            }
        },
        require('hapi-auth-jwt2')
    ])

    server.auth.strategy('jwt', 'jwt', HapiJWTConfig);
    server.auth.default('jwt');

    // Add the route

    const prefix = '/api'

    server.route([
        {
            method: 'GET',
            path: `/{path*}`,
            handler: {
                directory: {
                    path: Path.join(__dirname, 'client/build/index.html'),
                    listing: false,
                    index: true
                }
            },
            config: {
                auth: false
            }
        },
        {
            method:'POST',
            path:`${prefix}/login`,
            handler: authController.authenticate,
            config: {
                auth: false,
                description: 'Authentication',
                notes: 'Authentication route',
                tags: ['api', 'auth'],
                validate: {
                    payload: Joi.object({
                        email: Joi.string().email().required(),
                        password: Joi.string().required()
                    })
                }
            }
        },
        {
            method:'POST',
            path:`${prefix}/register`,
            handler: authController.registerUser,
            config: {
                auth: false,
                description: 'Registration',
                notes: 'Registration route',
                tags: ['api', 'auth']
                
            }
        },
        {
            method:'GET',
            path:`${prefix}/entries`,
            handler: entriesController.getAllUserEntries,
            config: {
                auth: 'jwt',
                description: 'Get All Entries',
                notes: 'retrieves all the entries under authorized user',
                tags: ['api', 'entry']
            }
        },
        {
            method:'GET',
            path:`${prefix}/entries/{id}`,
            handler: entriesController.getEntryById,
            config: {
                auth: 'jwt',
                description: 'Get Entry',
                tags: ['api', 'entry'],
                notes: 'Retrieves the data pertaining to the entry associated with user'
            }
        },
        {
            method:'POST',
            path:`${prefix}/entries`,
            handler: entriesController.postEntry,
            config: {
                auth: 'jwt',
                description: 'Post Entry',
                tags: ['api', 'entry'],
                notes: 'Inserts an entry into the database and creates a reference to authorized user'
            }
        },
        {
            method:'PUT',
            path:`${prefix}/entries/{id}`,
            handler: entriesController.updateEntry,
            config: {
                auth: 'jwt',
                description: 'Update Entry',
                tags: ['api', 'entry'],
                notes: 'Updates the entry with new values'
            }
        },
        {
            method:'DELETE',
            path:`${prefix}/entries/{id}`,
            handler: entriesController.deleteEntry,
            config: {
                auth: 'jwt',
                description: 'Delete Entry',
                tags: ['api', 'entry'],
                notes: 'Deletes the entry from database and removes reference from user\'s profile'
            }
        }
    ]);

    try {
        await server.start();
    }
    catch (err) {
        console.log(err);
        process.exit(1);
    }

    console.log('Server running at:', server.info.uri);
};

process.on('unHandledRejection', (err) => {
	if (err) {
		console.log(err);
		process.exit(1);
	}
});

start();