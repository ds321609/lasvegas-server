const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const UserSchema = new Schema({
    id: Schema.Types.ObjectId,
    email: String,
    password: String,
    entries: [{
       type: Schema.Types.ObjectId,
       ref: "Entry" 
    }],
    admin: Boolean


})

module.exports = mongoose.model('User', UserSchema)