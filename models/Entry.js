const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const EntrySchema = new Schema({
    date: Number,
    attributes: {
        looks: Number,
        adaptability: Number,
        strength: Number,
        value: Number,
        emotionConn: Number,
        goals: Number,
        authenticity: Number,
        selfWorth: Number
    },
    score: Number,
    id: Schema.Types.ObjectId
})

module.exports = mongoose.model('Entry', EntrySchema)